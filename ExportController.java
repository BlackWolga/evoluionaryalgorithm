package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class ExportController implements Initializable {
    @FXML
    private ComboBox exportFormat;

    @FXML
    private ComboBox ratioChooser;

    @FXML
    private TextField xSize;

    @FXML
    private TextField ySize;

    private int xPic;
    private int yPic;

    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        exportFormat.getItems().removeAll(exportFormat.getItems());
        exportFormat.getItems().addAll(".jpg", ".png", ".gif");
        exportFormat.getSelectionModel().select(".png");

        ratioChooser.getItems().removeAll(ratioChooser.getItems());
        ratioChooser.getItems().addAll("4:3", "16:9", "18:9", "21:9", "non-standard");
        ratioChooser.getSelectionModel().select("16:9");
    }

    @FXML
    public void enterXSize(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            xPic = Integer.parseInt(xSize.getText());
            switch (ratioChooser.getSelectionModel().getSelectedItem().toString()) {
                case "4:3":
                    ySize.setText(""+(xPic / 4 * 3));
                    break;
                case "16:9":
                    ySize.setText(""+(xPic / 16 * 9));
                    break;
                case "18:9":
                    ySize.setText(""+(xPic / 18 * 9));
                case "21:9":
                    ySize.setText(""+(xPic / 21 * 9));
                case "non-standard":
                    break;
            }


        }
    }

    @FXML
    public void enterYSize(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            yPic = Integer.parseInt(ySize.getText());
            switch (ratioChooser.getSelectionModel().getSelectedItem().toString()) {
                case "4:3":
                    xSize.setText(""+(yPic / 3 * 4));
                    break;
                case "16:9":
                    xSize.setText(""+(yPic / 9 * 16));
                    break;
                case "18:9":
                    xSize.setText(""+(yPic / 9 * 18));
                case "21:9":
                    xSize.setText(""+(yPic / 9 * 21));
                case "non-standard":
                    break;
            }


        }
    }

    public int getxPic() {
        return xPic;
    }

    public int getyPic() {
        return yPic;
    }

    public String getFormat() {
        return exportFormat.getSelectionModel().getSelectedItem().toString();
    }

    @FXML
    public void exportClick(ActionEvent event) {
//        for Paweł to implement saving the picture!!!
    }

}
