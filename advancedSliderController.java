package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Control;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.DoubleUnaryOperator;

public class advancedSliderController implements Initializable{


    private double nowAddPoly;

    @FXML
    private Slider sliderAddPoly;

    @FXML
    private Slider sliderDelPoly;

    @FXML
    private Slider sliderPolySize;

    @FXML
    private Slider sliderChangeCol;

    @FXML
    private Slider sliderAddVertex;

    @FXML
    private Slider sliderDelVertex;

    @FXML
    private Slider sliderShiftVertex;

    @FXML
    private TextField fieldAddPoly;

    @FXML
    private TextField fieldDelPoly;

    @FXML
    private TextField fieldPolySize;

    @FXML
    private TextField fieldChangeCol;

    @FXML
    private TextField fieldAddVertex;

    @FXML
    private TextField fieldDelVertex;

    @FXML
    private TextField fieldShiftVertex;


    public advancedSliderController() {
//        fieldAddPoly.setText(Double.toString(Controller.getNowChAddPol()));
//        fieldDelPoly.setText(Double.toString(Controller.getNowChDelPol()));
//        fieldPolySize.setText(Double.toString(Controller.getNowPolSize()));
//        fieldChangeCol.setText(Double.toString(Controller.getNowColChange()));
//        fieldAddVertex.setText(Double.toString(Controller.getNowChAddVer()));
//        fieldDelVertex.setText(Double.toString(Controller.getNowChDelVer()));
//        fieldShiftVertex.setText(Double.toString(Controller.getNowShVer()));
    }



    @Override
    public void initialize(URL var1, ResourceBundle var2) {
        sliderAddPoly.setValue(Controller.getNowChAddPol());
        sliderAddPoly.valueProperty().addListener((observable, oldValue, newValue) ->{
            if (!Controller.isRunning()) {
                Controller.setNowChAddPol(newValue.doubleValue());
                fieldAddPoly.setText(Double.toString(newValue.doubleValue()));
//                Controller.setNowChAddPol(nowAddPoly);
            }
        });
        sliderDelPoly.setValue(Controller.getNowChDelPol());
        sliderDelPoly.valueProperty().addListener((observable, oldValue, newValue) ->{
            if (!Controller.isRunning()) {
                Controller.setNowChDelPol(newValue.doubleValue());
                fieldDelPoly.setText(Double.toString(newValue.doubleValue()));
//                Controller.setNowChAddPol(nowAddPoly);
            }
        });
        sliderPolySize.setValue(Controller.getNowPolSize());
        sliderPolySize.valueProperty().addListener((observable, oldValue, newValue) ->{
            if (!Controller.isRunning()) {
                Controller.setNowPolSize(newValue.doubleValue());
                fieldPolySize.setText(Double.toString(newValue.doubleValue()));
//                Controller.setNowChAddPol(nowAddPoly);
            }
        });
        sliderChangeCol.setValue(Controller.getNowColChange());
        sliderChangeCol.valueProperty().addListener((observable, oldValue, newValue) ->{
            if (!Controller.isRunning()) {
                Controller.setNowColChange(newValue.doubleValue());
                fieldChangeCol.setText(Double.toString(newValue.doubleValue()));
//                Controller.setNowChAddPol(nowAddPoly);
            }
        });
        sliderAddVertex.setValue(Controller.getNowChAddVer());
        sliderAddVertex.valueProperty().addListener((observable, oldValue, newValue) ->{
            if (!Controller.isRunning()) {
                Controller.setNowChAddVer(newValue.doubleValue());
                fieldAddVertex.setText(Double.toString(newValue.doubleValue()));
//                Controller.setNowChAddPol(nowAddPoly);
            }
        });
        sliderDelVertex.setValue(Controller.getNowChDelVer());
        sliderDelVertex.valueProperty().addListener((observable, oldValue, newValue) ->{
            if (!Controller.isRunning()) {
                Controller.setNowChDelVer(newValue.doubleValue());
                fieldDelVertex.setText(Double.toString(newValue.doubleValue()));
//                Controller.setNowChAddPol(nowAddPoly);
            }
        });
        sliderShiftVertex.setValue(Controller.getNowShVer());
        sliderShiftVertex.valueProperty().addListener((observable, oldValue, newValue) ->{
            if (!Controller.isRunning()) {
                Controller.setNowShVer(newValue.doubleValue());
                fieldShiftVertex.setText(Double.toString(newValue.doubleValue()));
//                Controller.setNowChAddPol(nowAddPoly);
            }
        });

    }

//
//    start code for chnging NowChAddPoly
//

    @FXML
    protected void minusOneAddPoly(ActionEvent event) {
        if (!Controller.isRunning()) {
            if (Controller.getNowChAddPol() == 0)
                return;
            Controller.setNowChAddPol(Controller.getNowChAddPol()-0.05/100);
//        valueOne.setText(Integer.toString(nowAmount));
            fieldAddPoly.setText(Double.toString(Controller.getNowChAddPol()));
            sliderAddPoly.setValue(Controller.getNowChAddPol());
        }
    }

    @FXML
    protected void plusOneAddPoly(ActionEvent event) {
        if (!Controller.isRunning()) {
            if (Controller.getNowChAddPol() == 0.05) {
                return;
            }
            System.out.println(Controller.getNowChAddPol());
            Controller.setNowChAddPol(Controller.getNowChAddPol()+0.05/100);
            fieldAddPoly.setText(Double.toString(Controller.getNowChAddPol()));
            sliderAddPoly.setValue(Controller.getNowChAddPol());
            System.out.println(Controller.getNowChAddPol());
        }
    }

    @FXML
    protected void enterAddPoly(KeyEvent event) {
        if (!Controller.isRunning()) {
            if (event.getCode() == KeyCode.ENTER) {
                double pom = Double.parseDouble(fieldAddPoly.getText());
                if (pom < 0 || pom > 0.05)
                    return;
                Controller.setNowChAddPol(pom);
                sliderAddPoly.setValue(Controller.getNowChAddPol());
            }
        }
    }
//
//    end code for changing NowChAddPoly
//

//
//    start code for changing NowChDelPoly
//

    @FXML
    protected void minusOneDelPoly(ActionEvent event) {
        if (!Controller.isRunning()) {
            if (Controller.getNowChDelPol() == 0)
                return;
            Controller.setNowChDelPol(Controller.getNowChDelPol()-0.05/100);
//        valueOne.setText(Integer.toString(nowAmount));
            fieldDelPoly.setText(Double.toString(Controller.getNowChDelPol()));
            sliderDelPoly.setValue(Controller.getNowChDelPol());
        }
    }

    @FXML
    protected void plusOneDelPoly(ActionEvent event) {
        if (!Controller.isRunning()) {
            if (Controller.getNowChAddPol() == 0.05) {
                return;
            }
//            System.out.println(Controller.getNowChAddPol());
            Controller.setNowChDelPol(Controller.getNowChDelPol()+0.05/100);
            fieldDelPoly.setText(Double.toString(Controller.getNowChDelPol()));
            sliderDelPoly.setValue(Controller.getNowChDelPol());
//            System.out.println(Controller.getNowChAddPol());
        }
    }

    @FXML
    protected void enterDelPoly(KeyEvent event) {
        if (!Controller.isRunning()) {
            if (event.getCode() == KeyCode.ENTER) {
                double pom = Double.parseDouble(fieldDelPoly.getText());
                if (pom < 0 || pom > 0.05)
                    return;
                Controller.setNowChDelPol(pom);
                sliderDelPoly.setValue(Controller.getNowChDelPol());
            }
        }
    }
//
//    end code for changing NowChDelPoly
//

//
//    start code for changing size
//

    @FXML
    protected void minusOnePolySize(ActionEvent event) {
        if (!Controller.isRunning()) {
            if (Controller.getNowPolSize() <= 0) {
                Controller.setNowPolSize(0.0);
                return;
            }
            Controller.setNowPolSize(Controller.getNowPolSize()-0.02/100);
//        valueOne.setText(Integer.toString(nowAmount));
            fieldPolySize.setText(Double.toString(Controller.getNowPolSize()));
            sliderPolySize.setValue(Controller.getNowPolSize());
        }
    }

    @FXML
    protected void plusOnePolySize(ActionEvent event) {
        if (!Controller.isRunning()) {
            if (Controller.getNowPolSize() >= 0.02) {
                return;
            }
//            System.out.println(Controller.getNowChAddPol());
            Controller.setNowPolSize(Controller.getNowPolSize()+0.02/100);
            fieldPolySize.setText(Double.toString(Controller.getNowPolSize()));
            sliderPolySize.setValue(Controller.getNowPolSize());
//            System.out.println(Controller.getNowChAddPol());
        }
    }

    @FXML
    protected void enterPolySize(KeyEvent event) {
        if (!Controller.isRunning()) {
            if (event.getCode() == KeyCode.ENTER) {
                double pom = Double.parseDouble(fieldPolySize.getText());
                if (pom < 0 || pom > 0.02)
                    return;
                Controller.setNowPolSize(pom);
                sliderPolySize.setValue(Controller.getNowPolSize());
            }
        }
    }
//
//    end code for changing size
//

//
//    start code for changing colour
//

    @FXML
    protected void minusOneChangeCol(ActionEvent event) {
        if (!Controller.isRunning()) {
            if (Controller.getNowColChange() == 0)
                return;
            Controller.setNowColChange(Controller.getNowColChange()-0.05/100);
//        valueOne.setText(Integer.toString(nowAmount));
            fieldChangeCol.setText(Double.toString(Controller.getNowColChange()));
            sliderChangeCol.setValue(Controller.getNowColChange());
        }
    }

    @FXML
    protected void plusOneChangeCol(ActionEvent event) {
        if (!Controller.isRunning()) {
            if (Controller.getNowColChange() == 0.05) {
                return;
            }
//            System.out.println(Controller.getNowChAddPol());
            Controller.setNowColChange(Controller.getNowColChange()+0.05/100);
            fieldChangeCol.setText(Double.toString(Controller.getNowColChange()));
            sliderChangeCol.setValue(Controller.getNowColChange());
//            System.out.println(Controller.getNowChAddPol());
        }
    }

    @FXML
    protected void enterChangeCol(KeyEvent event) {
        if (!Controller.isRunning()) {
            if (event.getCode() == KeyCode.ENTER) {
                double pom = Double.parseDouble(fieldChangeCol.getText());
                if (pom < 0 || pom > 0.05)
                    return;
                Controller.setNowColChange(pom);
                sliderChangeCol.setValue(Controller.getNowColChange());
            }
        }
    }
//
//    end code for changing colour
//

//
//    start code for adding vertex
//

    @FXML
    protected void minusOneAddVertex(ActionEvent event) {
        if (!Controller.isRunning()) {
            if (Controller.getNowChAddVer() == 0)
                return;
            Controller.setNowChAddVer(Controller.getNowChAddVer()-0.05/100);
//        valueOne.setText(Integer.toString(nowAmount));
            fieldAddVertex.setText(Double.toString(Controller.getNowChAddVer()));
            sliderAddVertex.setValue(Controller.getNowChAddVer());
        }
    }

    @FXML
    protected void plusOneAddVertex(ActionEvent event) {
        if (!Controller.isRunning()) {
            if (Controller.getNowChAddVer() == 0.05) {
                return;
            }
//            System.out.println(Controller.getNowChAddPol());
            Controller.setNowChAddVer(Controller.getNowChAddVer()+0.05/100);
            fieldAddVertex.setText(Double.toString(Controller.getNowChAddVer()));
            sliderAddVertex.setValue(Controller.getNowChAddVer());
//            System.out.println(Controller.getNowChAddPol());
        }
    }

    @FXML
    protected void enterAddVertex(KeyEvent event) {
        if (!Controller.isRunning()) {
            if (event.getCode() == KeyCode.ENTER) {
                double pom = Double.parseDouble(fieldAddVertex.getText());
                if (pom < 0 || pom > 0.05)
                    return;
                Controller.setNowChAddVer(pom);
                sliderAddVertex.setValue(Controller.getNowChAddVer());
            }
        }
    }
//
//    end code for adding vertex
//

//
//    start code for deleting vertex
//

    @FXML
    protected void minusOneDelVertex(ActionEvent event) {
        if (!Controller.isRunning()) {
            if (Controller.getNowChDelVer() == 0)
                return;
            Controller.setNowChDelVer(Controller.getNowChDelVer()-0.05/100);
//        valueOne.setText(Integer.toString(nowAmount));
            fieldDelVertex.setText(Double.toString(Controller.getNowChDelVer()));
            sliderDelVertex.setValue(Controller.getNowChDelVer());
        }
    }

    @FXML
    protected void plusOneDelVertex(ActionEvent event) {
        if (!Controller.isRunning()) {
            if (Controller.getNowChDelVer() == 0.05) {
                return;
            }
//            System.out.println(Controller.getNowChAddPol());
            Controller.setNowChDelVer((Controller.getNowChDelVer()+0.05/100));
            fieldDelVertex.setText(Double.toString(Controller.getNowChDelVer()));
            sliderDelVertex.setValue(Controller.getNowChDelVer());
//            System.out.println(Controller.getNowChAddPol());
        }
    }

    @FXML
    protected void enterDelVertex(KeyEvent event) {
        if (!Controller.isRunning()) {
            if (event.getCode() == KeyCode.ENTER) {
                double pom = Double.parseDouble(fieldDelVertex.getText());
                if (pom < 0 || pom > 0.05)
                    return;
                Controller.setNowChDelVer(pom);
                sliderDelVertex.setValue(Controller.getNowChDelVer());
            }
        }
    }
//
//    end code for deleting vertex
//

//
//    start code for shifting vertex
//

    @FXML
    protected void minusOneShiftVertex(ActionEvent event) {
        if (!Controller.isRunning()) {
            if (Controller.getNowShVer() == 0)
                return;
            Controller.setNowShVer(Controller.getNowShVer()-0.05/100);
//        valueOne.setText(Integer.toString(nowAmount));
            fieldShiftVertex.setText(Double.toString(Controller.getNowShVer()));
            sliderShiftVertex.setValue(Controller.getNowShVer());
        }
    }

    @FXML
    protected void plusOneShiftVertex(ActionEvent event) {
        if (!Controller.isRunning()) {
            if (Controller.getNowShVer() == 0.05) {
                return;
            }
//            System.out.println(Controller.getNowChAddPol());
            Controller.setNowShVer((Controller.getNowShVer()+0.05/100));
            fieldShiftVertex.setText(Double.toString(Controller.getNowShVer()));
            sliderShiftVertex.setValue(Controller.getNowShVer());
//            System.out.println(Controller.getNowChAddPol());
        }
    }

    @FXML
    protected void enterShiftVertex(KeyEvent event) {
        if (!Controller.isRunning()) {
            if (event.getCode() == KeyCode.ENTER) {
                double pom = Double.parseDouble(fieldShiftVertex.getText());
                if (pom < 0 || pom > 0.05)
                    return;
                Controller.setNowShVer(pom);
                sliderShiftVertex.setValue(Controller.getNowShVer());
            }
        }
    }
//
//    end code for shifting vertex
//

}


