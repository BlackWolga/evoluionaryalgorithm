package sample;


import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.ResourceBundle;


public class Controller implements Initializable {

    final double AMOUNT;
    final double ADDPOLYCHANCE;
    final double DELPOLYCHANCE;
    final double POLYSIZE;
    final double COLCHANGE;
    final double VERTEXADD;
    final double VERTEXDEL;
    final double VERTEXSHIFT;
    final double ROI;

    private static double nowAmount;
    private static double nowChAddPol;
    private static double nowChDelPol;
    private static double nowPolSize;
    private static double nowColChange;
    private static double nowChAddVer;
    private static double nowChDelVer;
    private static double nowShVer;
    private static double nowRoi;
    private static int nowPopSize;
    private static int nowNumParents;
    private static int nowScaling;
    private static int xChartCount;




    private static boolean running;
    private int imgNum;

    @FXML
    private TextField fieldOne;

//    @FXML
//    private TextField fieldOne1;
//
//    @FXML
//    private TextField fieldOne2;
//
//    @FXML
//    private TextField fieldOne3;
//
//    @FXML
//    private TextField fieldOne4;
//
//    @FXML
//    private TextField fieldOne5;
//
//    @FXML
//    private TextField fieldOne6;
//
//    @FXML
//    private TextField fieldOne7;

    @FXML
    private TextField fieldOne8;

    @FXML
    private TextField fieldPopSize;

    @FXML
    private TextField fieldNumParents;

    @FXML
    private TextField fieldScaling;

    @FXML
    private Slider slideOne;

    @FXML
    private Slider slideOne1;

    @FXML
    private Slider slideOne8;

    @FXML
    private Slider slidePopSize;

    @FXML
    private Slider slideNumParents;

    @FXML
    private Slider slideScaling;

    @FXML
    private ImageView rImage;

    @FXML
    private ImageView lImage;

    @FXML
    private NumberAxis xAxis;

    @FXML
    private NumberAxis yAxis;

    @FXML
    private LineChart<Number, Number> Ch;

    private LineChart.Series<Number, Number> series = new LineChart.Series<Number, Number>();

    public Controller() {
        AMOUNT = 0.05;
        ADDPOLYCHANCE = 0.05;
        DELPOLYCHANCE = 0.05;
        POLYSIZE = 0.2;
        COLCHANGE = 0.05;
        VERTEXADD = 0.05;
        VERTEXDEL = 0.02;
        VERTEXSHIFT = 0.05;
        ROI = 0.03125;
        nowAmount = 0;
        nowChAddPol = 0;
        nowChDelPol = 0;
        nowPolSize = 0;
        nowColChange = 0;
        nowChAddVer = 0;
        nowChDelVer = 0;
        nowShVer = 0;
        nowPopSize = 10;
        nowNumParents = 1;
        nowScaling = 1;
        nowRoi = 1/32;
        running = false;
        imgNum = 0;
        xChartCount = 1;
    }

    public static boolean isRunning() {
        return running;
    }

    public static double getNowAmount() {
        return nowAmount;
    }

    public static double getNowChAddPol() {
        return nowChAddPol;
    }

    public static double getNowChDelPol() {
        return nowChDelPol;
    }

    public static double getNowPolSize() {
        return nowPolSize;
    }

    public static double getNowColChange() {
        return nowColChange;
    }

    public static double getNowChAddVer() {
        return nowChAddVer;
    }

    public static double getNowChDelVer() {
        return nowChDelVer;
    }

    public static double getNowShVer() {
        return nowShVer;
    }

    public static double getNowRoi() {
        return nowRoi;
    }

    public static int getNowPopSize() {
        return nowPopSize;
    }

    public static int getNowNumParents() {
        return nowNumParents;
    }

    public static int getNowScaling() {
        return nowScaling;
    }

    public static void setNowAmount(double nowAmount) {
        Controller.nowAmount = nowAmount;
    }

    public static void setNowChAddPol(double nowChAddPol) {
        Controller.nowChAddPol = nowChAddPol;
    }

    public static void setNowChDelPol(double nowChDelPol) {
        Controller.nowChDelPol = nowChDelPol;
    }

    public static void setNowPolSize(double nowPolSize) {
        Controller.nowPolSize = nowPolSize;
    }

    public static void setNowColChange(double nowColChange) {
        Controller.nowColChange = nowColChange;
    }

    public static void setNowChAddVer(double nowChAddVer) {
        Controller.nowChAddVer = nowChAddVer;
    }

    public static void setNowChDelVer(double nowChDelVer) {
        Controller.nowChDelVer = nowChDelVer;
    }

    public static void setNowShVer(double nowShVer) {
        Controller.nowShVer = nowShVer;
    }

    public static void setNowRoi(double nowRoi) {
        Controller.nowRoi = nowRoi;
    }

    public static void setNowPopSize(int nowPopSize) {
        Controller.nowPopSize = nowPopSize;
    }

    public static void setNowNumParents(int nowNumParents) {
        Controller.nowNumParents = nowNumParents;
    }

    public static void setNowScaling(int nowScaling) {
        Controller.nowScaling = nowScaling;
    }

    @Override
    public void initialize(URL var1, ResourceBundle var2) {
//        initialize the chart
        xAxis.setAutoRanging(false);
        xAxis.setLowerBound(0);
        xAxis.setUpperBound(10);
        xAxis.setTickUnit(1);

        yAxis.setAutoRanging(false);
        yAxis.setLowerBound(0);
        yAxis.setUpperBound(1.2);
        yAxis.setTickUnit(0.1);

        series.getData().add(new LineChart.Data<Number, Number>(xChartCount, 0.8));
        xChartCount++;
        Ch.setCreateSymbols(false);
        Ch.setAnimated(false);
        Ch.getData().add(series);

        slideOne.setMax(AMOUNT);
        slideOne.setBlockIncrement(AMOUNT/100);
        fieldOne.setText("0.0");
        slideOne.valueProperty().addListener((observable, oldValue, newValue) ->{
            if (!running) {
                nowAmount = newValue.doubleValue();
                fieldOne.setText(Double.toString(newValue.doubleValue()));
            }
        });


        slideOne8.setBlockIncrement(ROI);
        slideOne8.setMax(1);
        slideOne8.setMin(0.03125);
        fieldOne8.setText("0.03125");
        slideOne8.valueProperty().addListener((observable, oldValue, newValue) ->{
            if (!running) {
                nowRoi = newValue.doubleValue();
                fieldOne8.setText(Double.toString(newValue.doubleValue()));
            }
        });

        fieldPopSize.setText("10");
        slidePopSize.valueProperty().addListener((observable, oldValue, newValue) ->{
            if (!running) {
                nowPopSize = newValue.intValue();
                fieldPopSize.setText(Integer.toString(newValue.intValue()));
            }
        });

        fieldNumParents.setText("1");
        slideNumParents.valueProperty().addListener((observable, oldValue, newValue) ->{
            if (!running) {
                nowNumParents = newValue.intValue();
                fieldNumParents.setText(Integer.toString(newValue.intValue()));
            }
        });

        fieldScaling.setText("1");
        slideScaling.valueProperty().addListener((observable, oldValue, newValue) ->{
            if (!running) {
                nowScaling = newValue.intValue();
                fieldScaling.setText(Integer.toString(newValue.intValue()));
            }
        });
    }



    //    function to implement the evolutionary algorithm
    @FXML
    protected void startProcessing(ActionEvent event) {
//        sample code here for now
        if (!running) {
//            Image constImg = createImg(polygons);

            Image img = new Image("sample/img/sw.jpeg");
            this.lImage.setImage(img);
            this.lImage.setFitWidth(320);
            this.lImage.setPreserveRatio(true);
            running = true;
//            System.out.println(slideOne.getMax());
        }
    }

//
    @FXML
    protected void resetBut(ActionEvent event) {
        if (!running) {
            nowAmount = 0.02;
            System.out.println("bla");
            slideOne.setValue(nowAmount);
            fieldOne.setText(Double.toString(nowAmount));
        }
    }

    @FXML
    protected void stopProcessing(ActionEvent event) throws IOException {
        if (running) {
            System.out.println("STOP");
            saveImg();
            running = false;
        }
    }

//
//    start code for first value
//
    @FXML
    protected void minusOne(ActionEvent event) {
        if (!running) {
            if (nowAmount == 0)
                return;
            nowAmount-=(AMOUNT/100);
//        valueOne.setText(Integer.toString(nowAmount));
            fieldOne.setText(Double.toString(nowAmount));
            slideOne.setValue(nowAmount);
        }
    }

    @FXML
    protected void plusOne(ActionEvent event) {
        if (!running) {
            if (nowAmount == AMOUNT)
                return;
            nowAmount+=(AMOUNT/100);
//        valueOne.setText(Integer.toString(nowAmount));
            fieldOne.setText(Double.toString(nowAmount));
            slideOne.setValue(nowAmount);
        }
    }

    @FXML
    protected void enterHandle(KeyEvent event) {
        if (!running) {
            if (event.getCode() == KeyCode.ENTER) {
                double pom = Double.parseDouble(fieldOne.getText());
                if (pom < 0 || pom > AMOUNT)
                    return;
                nowAmount = pom;
                slideOne.setValue(nowAmount);
            }
        }
    }
//
//    end code for first value
//

//
//  start code for ninth value
//
    @FXML
    protected void minusOne8(ActionEvent event) {
        if (!running) {
            if (nowRoi == 0.03125)
                return;
            nowRoi-=ROI;
//        valueOne.setText(Integer.toString(nowAmount));
            fieldOne8.setText(Double.toString(nowRoi));
            slideOne8.setValue(nowRoi);
        }
    }

    @FXML
    protected void plusOne8(ActionEvent event) {
        if (!running) {
            if (nowRoi == 1)
                return;
            nowRoi+=ROI;
//        valueOne.setText(Integer.toString(nowAmount));
            fieldOne8.setText(Double.toString(nowRoi));
            slideOne8.setValue(nowRoi);
        }
    }

    @FXML
    protected void enterHandle8(KeyEvent event) {
        if (!running) {
            if (event.getCode() == KeyCode.ENTER) {
                double pom = Double.parseDouble(fieldOne8.getText());
                if (pom < 0 || pom > 1)
                    return;
                nowRoi = pom;
                slideOne8.setValue(nowRoi);
            }
        }
    }
//
//    end code for ninth value
//

    //
//    start code for Population Size
//
    @FXML
    protected void minusOnePopSize(ActionEvent event) {
        if (!running) {
            if (nowPopSize == 10)
                return;
            nowPopSize--;
//        valueOne.setText(Integer.toString(nowAmount));
            fieldPopSize.setText(Integer.toString(nowPopSize));
            slidePopSize.setValue(nowPopSize);
        }
    }

    @FXML
    protected void plusOnePopSize(ActionEvent event) {
        if (!running) {
            if (nowPopSize == 1000)
                return;
            nowPopSize++;
//        valueOne.setText(Integer.toString(nowAmount));
            fieldPopSize.setText(Integer.toString(nowPopSize));
            slidePopSize.setValue(nowPopSize);
        }
    }

    @FXML
    protected void enterPopSize(KeyEvent event) {
        if (!running) {
            if (event.getCode() == KeyCode.ENTER) {
                int pom = Integer.parseInt(fieldPopSize.getText());
                if (pom < 10 || pom > 1000)
                    return;
                nowPopSize = pom;
                slidePopSize.setValue(nowPopSize);
            }
        }
    }
//
//    end code for Population Size
//
//
//    start code for Number of Parents
//
    @FXML
    protected void minusOneNumParents(ActionEvent event) {
        if (!running) {
            if (nowNumParents == 1)
                return;
            nowNumParents--;
//          valueOne.setText(Integer.toString(nowAmount));
            fieldNumParents.setText(Integer.toString(nowNumParents));
            slideNumParents.setValue(nowNumParents);
        }
    }

    @FXML
    protected void plusOneNumParents(ActionEvent event) {
        if (!running) {
            if (nowNumParents == 100)
                return;
            nowNumParents++;
//        valueOne.setText(Integer.toString(nowAmount));
            fieldNumParents.setText(Integer.toString(nowNumParents));
            slideNumParents.setValue(nowNumParents);
        }
    }

    @FXML
    protected void enterNumParents(KeyEvent event) {
        if (!running) {
            if (event.getCode() == KeyCode.ENTER) {
                int pom = Integer.parseInt(fieldNumParents.getText());
                if (pom < 1 || pom > 100)
                    return;
                nowNumParents = pom;
                slideNumParents.setValue(nowNumParents);
            }
        }
    }

//
//    end code for Number of Parents
//


//
//    start code for Scaling the image
//
@FXML
protected void minusOneScaling(ActionEvent event) {
    if (!running) {
        if (nowScaling == 1)
            return;
        nowScaling--;
//          valueOne.setText(Integer.toString(nowAmount));
        fieldScaling.setText(Integer.toString(nowScaling));
        slideScaling.setValue(nowScaling);
    }
}

    @FXML
    protected void plusOneScaling(ActionEvent event) {
        if (!running) {
            if (nowScaling == 100)
                return;
            nowScaling++;
//        valueOne.setText(Integer.toString(nowAmount));
            fieldScaling.setText(Integer.toString(nowScaling));
            slideScaling.setValue(nowScaling);
        }
    }

    @FXML
    protected void enterScaling(KeyEvent event) {
        if (!running) {
            if (event.getCode() == KeyCode.ENTER) {
                int pom = Integer.parseInt(fieldScaling.getText());
                if (pom < 1 || pom > 100)
                    return;
                nowScaling = pom;
                slideScaling.setValue(nowScaling);
            }
        }
    }

//
//    end of code for scaling the image
//

    @FXML
    protected void advancedButton(ActionEvent event) {
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("advancedSliders.fxml"));
            Stage stage = new Stage();
            stage.setTitle("My New Stage Title");
            stage.setScene(new Scene(root, 450, 450));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    protected void export(ActionEvent event) throws IOException {
        Parent root;
            root = FXMLLoader.load(getClass().getResource("exportPopup.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Choose details to export");
            stage.setScene(new Scene(root, 600, 300));
            stage.show();
    }

    @FXML
    protected void chartUpd() {
        if (series.getData().size() > xAxis.getUpperBound()) {
            xAxis.setUpperBound(xAxis.getUpperBound()+1);
        }
        series.getData().add(new LineChart.Data<Number, Number>(xChartCount, 0.8));
        Ch.getData().set(0, series);
        xChartCount++;
    }


    @FXML
    protected void open(ActionEvent event) throws IOException {
        if (!running) {

            String dir;
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText("Choose Images from img/ directory of the project package");
            alert.setContentText("other dirs will probably throw an Exception");
            alert.showAndWait();

//        opens new dir explorer
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choose Reference Image");
//            fileChooser.setInitialDirectory(new File("/media/ig-88/BE1854CB18548475/Moje_Projekty/Java/JavaFXTutorials/sceNeBuilderDemo/src/sample/img/"));
            File file = fileChooser.showOpenDialog(rImage.getScene().getWindow());
            if (file != null) {
                dir = file.getCanonicalPath();
                String tab[] = dir.split("/");
                dir = tab[tab.length-1];
                loadrImage(dir);
            }
        }
    }

    protected void loadrImage(String dir){
//      you can choose images only from /sample/img directory :/
//        Image img = new Image("sample/sw.jpeg");
        Image img = new Image("sample/img/"+dir);
        this.rImage.setImage(img);
        this.rImage.setFitWidth(320);
        this.rImage.setPreserveRatio(true);
        System.out.println(this.rImage.getX()+" "+this.rImage.getY()+" "+this.rImage.getScaleX());
        System.out.println("opened");
    }

    private void saveImg() throws IOException{
        BufferedImage bufIImg = SwingFXUtils.fromFXImage(lImage.getImage(), null);
        ImageIO.write(bufIImg, "jpeg", new File("src/sample/OUT/outTest"+this.imgNum+".jpeg"));
    }

    public double[] getValues(){
        double[] pom = {nowAmount, nowChAddPol, nowChDelPol, nowPolSize, nowColChange, nowChAddVer, nowChDelVer, nowShVer, nowRoi};
        return pom;
    }

}
