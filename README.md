Some example functionalities:

In Menu, after clicking Open you can choose an image to display on right side

After pressing Start button an image will appear on left side and all buttons will be blocked

After pressing Stop buttons will be enabled again and the picture on the left will be saved to OUT directory that should be created in sample/ folder before running the project